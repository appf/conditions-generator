'use strict'
const formElem = document.getElementById('myForm')
const alertTemplate = document.getElementById('alertTemplate')
const infoArea = document.getElementById('infoArea')
const btnElem = document.getElementById('submit-btn')
const spinnnerElem = document.getElementById('submit-btn')

function handleErrors (response) {
  if (response.ok) return response

  response.json().then((responseData) => {
    if (response.status === 400) {
      const alert = alertTemplate.content.firstElementChild.cloneNode(true)
      alert.classList.add('alert-warning')
      alert.getElementsByClassName('textarea')[0].textContent = responseData.error
      infoArea.appendChild(alert)
      setTimeout(() => {
        btnElem.classList.remove('disabled')
        spinnnerElem.classList.remove('spinner-grow')
        spinnnerElem.value = 'Submit'
      }, 2000)
    }
    if (response.status === 422) {
      const alert = alertTemplate.content.firstElementChild.cloneNode(true)
      alert.classList.add('alert-warning')
      alert.getElementsByClassName('textarea')[0].textContent = responseData.error
      infoArea.appendChild(alert)
      setTimeout(() => {
        btnElem.classList.remove('disabled')
        spinnnerElem.classList.remove('spinner-grow')
        spinnnerElem.value = 'Submit'
      }, 2000)
    }
    if (response.status === 500) {
      const alert = alertTemplate.content.firstElementChild.cloneNode(true)
      alert.classList.add('alert-danger')
      alert.getElementsByClassName('textarea')[0].textContent = responseData.error
      infoArea.appendChild(alert)
      setTimeout(() => {
        btnElem.classList.remove('disabled')
        spinnnerElem.classList.remove('spinner-grow')
        spinnnerElem.value = 'Submit'
      }, 2000)
    }
  }).catch((e) => {
    const alert = alertTemplate.content.firstElementChild.cloneNode(true)
    alert.classList.add('alert-danger')
    alert.getElementsByClassName('textarea')[0].textContent = response.statusText
    infoArea.appendChild(alert)
    setTimeout(() => {
      btnElem.classList.remove('disabled')
      spinnnerElem.classList.remove('spinner-grow')
      spinnnerElem.value = 'Submit'
    }, 2000)
  })
  return false
}

if (formElem) {
  formElem.onsubmit = async (e) => {
    const btnElem = document.getElementById('submit-btn')
    const spinnnerElem = document.getElementById('submit-btn')
    spinnnerElem.classList.add('spinner-grow')
    spinnnerElem.value = ''
    btnElem.classList.add('disabled')
    e.preventDefault()

    const formData = new FormData(formElem)
    fetch(formElem.action, {
      method: 'POST',
      body: formData
    })
      .then(handleErrors)
      .then((response) => {
        if (!response) return
        response.blob().then((blob) => {
          const anchor = document.createElement('a')
          if (response.headers.get('Content-Disposition') !== null) {
            anchor.download = response.headers.get('Content-Disposition').split('filename=')[1]
          }
          if (response.headers.get('content-disposition') !== null) {
            anchor.download = response.headers.get('content-disposition').split('filename=')[1]
          }
          let contentType = 'text/plain'

          if (response.headers.get('Content-Type') !== null) {
            contentType = response.headers.get('Content-Type')
          }
          if (response.headers.get('content-type') !== null) {
            contentType = response.headers.get('content-type')
          }

          anchor.href = (window.webkitURL || window.URL).createObjectURL(blob)
          anchor.dataset.downloadurl = [contentType, anchor.download, anchor.href].join(':')
          anchor.click()
          setTimeout(() => {
            btnElem.classList.remove('disabled')
            spinnnerElem.classList.remove('spinner-grow')
            spinnnerElem.value = 'Submit'
          }, 2000)
        })
      })
      .catch((e) => {
        const alert = alertTemplate.content.firstElementChild.cloneNode(true)
        alert.classList.add('alert-danger')
        alert.getElementsByClassName('textarea')[0].textContent = e
        infoArea.appendChild(alert)
        setTimeout(() => {
          btnElem.classList.remove('disabled')
          spinnnerElem.classList.remove('spinner-grow')
          spinnnerElem.value = 'Submit'
        }, 2000)
      })
  }
}
