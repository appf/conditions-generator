---
title: 'Environments'
date: 2021-07-02T14:48:01+10:00
draft: false
weight: 4
summary: Information about individual controlled growth environments
---

###### PSI Growth Capsules

Controller git repo: https://gitlab.com/appf-anu/controller-capsule

Capsules have and A side and a B side, internally referenced as section1 and section2.


###### Conviron Growth Chambers

Controller git repo: https://gitlab.com/appf-anu/controller-conviron2

**Do not try and control these chambers using the conditions files generated here**, you will become very, very sad like me.
