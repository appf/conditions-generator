---
title: 'Lights'
date: 2021-07-02T14:48:01+10:00
draft: false
weight: 3
summary: Information about supported lights, their channels & wavelengths
---

## Channels & Wavelengths

This is the list of available channels & wavelengths for the different kinds of lights this tool supports. This will aid you in writing the *Light Scaling* parameters.

###### PSI Growth Capsule

Controller git repo: https://gitlab.com/appf-anu/controller-capsule

1. coolwhite
2. deepred
3. farred


###### PSI Growth Capsule Fancy

Controller git repo: https://gitlab.com/appf-anu/controller-capsule

1. coolwhite
2. deepred
3. farred
4. royalblue
5. blue
6. cyan

###### HelioSpectra Dyna

Controller git repo: https://gitlab.com/appf-anu/controller-heliospectra2

1. 380nm
2. 400nm
3. 420nm
4. 450nm
5. 530nm
6. 620nm
7. 660nm
8. 735nm
9. 5700k
    
###### HelioSpectra S10

Controller git repo: https://gitlab.com/appf-anu/controller-heliospectra2

1. 370nm
2. 400nm
3. 420nm
4. 450nm
5. 530nm
6. 620nm
7. 660nm
8. 735nm
9. 850nm
10. 6500k


###### HelioSpectra S7

Controller git repo: https://gitlab.com/appf-anu/controller-heliospectra2

1. 400nm
2. 420nm
3. 450nm
4. 530nm
5. 630nm
6. 660nm
7. 735nm


###### PSI FytoPanel

Controller git repo: https://gitlab.com/appf-anu/controller-fytopanel

1. coolwhite
2. blue
3. green
4. red
5. deepred
6. farred
7. infrared
8. cyan


###### Conviron

Controller git repo (**DONT DO IT**): https://gitlab.com/appf-anu/controller-conviron2

1. light1
2. light2
