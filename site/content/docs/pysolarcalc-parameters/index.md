---
title: 'PySolarCalc Parameters'
date: 2021-07-02T14:10:07+10:00
draft: false
weight: 4
summary: Parameters configure the resulting run conditions.
---


### Light Type

This parameter configures the type of light to generate conditions/headers for.


### Range Actual Start | Start | End

These three parameters control the desired start and end dates for the simulation.

They can be specified as a **date** or a **datetime** (although using a **date** only is recommended).

*Range End* must be chronologically after *Range Start*

*Actual Start* adjusts the "datetime" column so that the simulation begins from this date.


### Duration

`default: 1d`

This parameter controls the range the simulation should cover, starting at *Range Start*. It overrides the *Range End* parameter.

It should be specified as a **duration**.


### Scale Factor

This is a multiplier that accounts for the fact that a chamber cannot produce the quantity of light the sun does, and allows one to optimise spectra assuming that the sun was *Scale Factor* times as bright. 

Defaults to 0.5, which is about right for our high light chambers. 

It should be set to approximately: 

```python
max(chamber par) / max(sun)
```

Should be specified as a **float**.


## Types

This document contains references to multiple types of values

* **duration**: a length of time, specified like "1m5s" for 1 minute and 5 seconds. Does not support fractional values like "1.5m". Supported unit prefixes are w,d,h,m,s for week, day, hour, minute, second respectively.

* **date** [ISO8601](https://en.wikipedia.org/wiki/ISO_8601) date format. example: 2021-07-16 for July 16th, 2021

* **datetime** [ISO8601](https://en.wikipedia.org/wiki/ISO_8601) datetime format. example: 2021-07-16T10:31:12 for July 16th, 2021 10:31:12 am.

* **time** a date naive time, does not support military time ("1800"). example: "6pm", "18:00".

* **float** a base 10 number with a decimal point. example: "10.82"

* **integer** a base 10 number without a decimal point. example: "14"