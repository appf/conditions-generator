---
title: 'Simple Parameters'
date: 2021-07-02T14:10:07+10:00
draft: false
weight: 1
summary: Parameters configure the resulting run conditions.
---


### Light Type

This parameter configures the type of light to generate conditions/headers for.

All lights will be set to 100% during the day and 0% at night.


### Range Start | End

These two parameters control the desired start and end dates of the conditions in the conditions file.

They can be specified as a **date** or a **datetime**

If you want to use *Range End*, empty *Duration*

*Range End* must be chronologically after *Range Start*. 


### Duration

`default: 1d`

This parameter controls how long the conditions file should run for. Supplying a value for this parameter causes the *Range End* parameter to be ignored.

It should be specified as a **duration**.


### Sunrise | Photoperiod

*Sunrise* controls the beginning of the day, specified as a **time**.

*Photoperiod* determines the length of the day, starting at *Sunrise*, specified as a **duration**.


### Temperature Model

Allows you to select the model used to calculate the temperature for the day.

### Temperature High | Low

*Temperature High* controls the day temperature, in degrees Celsius, as a **float**.

*Temperature Low* controls the night temperature, in degrees Celsius, as a **float**.

Precision over 2 decimal places will be rounded to 2 decimal places.


### Humidity Model

Allows you to change the way that humidity is calculated. If you select [Complex (Allen)]({{< ref "/docs/models/index.md#allen-1985" >}}), *Humidity High* and *Humidity Low* have no effect.


### Humidity High | Low

*Humidity High* controls the day/high value for humidity, as % Relative Humidity, as an **integer**.

*Humidity Low* controls the night/low value for humidity, as % Relative Humidity, as an **integer**.

Floating point numbers will floored to next lowest whole number. Using the [Complex (Allen)]({{< ref "/docs/models/index.md#allen-1985" >}}) *Humidity Model* causes both these parameters to be ignored.


### Light Scaling

This parameter controls the multiplier for the daytime light values. 

It can be specified as either a comma separated list of **float**s that will be applied on a per channel/wavelength basis or a single **float** that is applied to all light channels/wavelengths.

If this is a comma separated list of floats, it must match the length of the *Light Type*s available channels/wavelengths. For a list of channel/wavelengths per light, see [Lights]({{< ref "/docs/lights/index.md" >}})


## Types

This document contains references to multiple types of values

* **duration**: a length of time, specified like "1m5s" for 1 minute and 5 seconds. Does not support fractional values like "1.5m". Supported unit prefixes are w,d,h,m,s for week, day, hour, minute, second respectively.

* **date** [ISO8601](https://en.wikipedia.org/wiki/ISO_8601) date format. example: 2021-07-16 for July 16th, 2021

* **datetime** [ISO8601](https://en.wikipedia.org/wiki/ISO_8601) datetime format. example: 2021-07-16T10:31:12 for July 16th, 2021 10:31:12 am.

* **time** a date naive time, does not support military time ("1800"). example: "6pm", "18:00".

* **float** a base 10 number with a decimal point. example: "10.82"

* **integer** a base 10 number without a decimal point. example: "14"