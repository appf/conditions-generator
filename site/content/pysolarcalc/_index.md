---
title: 'PySolarCalc'
date: 2018-11-28T15:14:39+10:00
weight: 2
form: pysolarcalc_form
---

Follows formulae from
[Spokas and Forcella (2006)](https://pubag.nal.usda.gov/catalog/1910)